export const isLoading = state => state.global.isLoading
export const isSideNavHidden = state => state.global.sideNav.isHidden
export const isAuthenticated = state => state.auth.isAuthenticated
