import { SET_AUTH } from '../mutation-types'
import { LOGIN } from '../actions'
import apiService from '../../common/api-service'
import jwtService from '../../common/jwt-service'
import swal from 'sweetalert2'

// initial state
const state = {
  isAuthenticated: false,
  userData: null
}

const actions = {
  [LOGIN]: (context, credentials) => {
    return new Promise((resolve, reject) => {
      apiService
        .post('login', credentials)
        .then(({ data }) => {
          context.commit(SET_AUTH, data)
          resolve(data)
        })
        .catch(err => {
          swal({
            type: 'error',
            title: 'Oops...',
            text: 'E-mail ou senha incorretos.'
          })
          throw new Error(err)
        })
    })
  }
}

// getters
const getters = {
  userData: state => state.userData
}

// mutations
const mutations = {
  [SET_AUTH] (state, userData) {
    state.isAuthenticated = true
    state.userData = userData.user
    jwtService.saveToken(userData.token)
    apiService.setHeader()
  }
}

export default {
  state,
  actions,
  getters,
  mutations
}
