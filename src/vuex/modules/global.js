import * as types from '../mutation-types'

import {
  TOGGLE_SIDENAV,
  SIDENAV_IS_HIDDEN,
  TOGGLE_LOADER
} from '../actions'

// initial state
const state = {
  isLoading: false,
  sideNav: {
    isHidden: true
  }
}

// actions
const actions = {
  [TOGGLE_SIDENAV]: (context) => {
    context.commit(TOGGLE_SIDENAV)
  },
  [SIDENAV_IS_HIDDEN]: (context) => {
    context.commit(SIDENAV_IS_HIDDEN)
  },
  [TOGGLE_LOADER]: (context) => {
    context.commit(TOGGLE_LOADER)
  }
}

// getters
const getters = {
  sideNavStatus: state => state.sideNav.isHidden
}

// mutations
const mutations = {
  [types.TOGGLE_SIDENAV]: (state) => (state.sideNav.isHidden = !state.sideNav.isHidden),
  [types.SIDENAV_IS_HIDDEN]: (state) => (state.sideNav.isHidden = !state.sideNav.isHidden),
  [types.TOGGLE_LOADER]: (state) => (state.isLoading = !state.isLoading)
}

export default {
  state,
  actions,
  getters,
  mutations
}
