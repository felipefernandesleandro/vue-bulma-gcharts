import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters'

/**
 * Modules
 */
import global from './modules/global'
import auth from './modules/auth'

Vue.use(Vuex)

export default new Vuex.Store({
  getters,
  modules: {
    global,
    auth
  }
})
